package com.springhomework.demo.service;

import com.springhomework.demo.model.Article;
import com.springhomework.demo.model.Category;

import java.util.List;

public interface ArticleService {
    List<Article> findAll();
    Article findById(int id);
    void delete(int id);
    void update(Article article);
    void saveArticle(Article article);
    List<Article> search(String keyword);
    List<Article> getPage(int limit, int offset);
}
