package com.springhomework.demo.repository;

import com.springhomework.demo.model.Category;
import com.springhomework.demo.repository.Provider.CategoryProvider;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository
{
    @SelectProvider(method = "addCategory", type = CategoryProvider.class)
    void addCategory(Category category);

    @SelectProvider(method = "deleteCategory",type = CategoryProvider.class)
    Category deleteCategory(int id);

    @SelectProvider(method = "updateCategory",type = CategoryProvider.class)
    void updateCategory(Category category);

    @SelectProvider(method = "findAllCat",type = CategoryProvider.class)
    List<Category> findAllCat();

    @SelectProvider(method = "findAllCat",type = CategoryProvider.class)
    Category findByIdCat(int id);


}
