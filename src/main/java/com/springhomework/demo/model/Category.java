package com.springhomework.demo.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Category
{
    private int id;

    @NotBlank(message = "this author is required")
    @Size(min=3, max =8,message = "min 3 and max 8")
    private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }
}
