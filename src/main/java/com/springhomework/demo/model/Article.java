package com.springhomework.demo.model;

import javax.validation.constraints.*;

public class Article {
    private Integer id;

    @NotBlank
    private String title;
    private String description;

    @NotBlank(message = "this author is required")
    @Size(min=3, max =8,message = "min 3 and max 8")
    private String author;
    private String createDate;
    private String thumbnail;

    private String cateID;

    public Article(Integer id, @NotBlank String title, String description, @NotBlank(message = "this author is required") @Size(min = 3, max = 8, message = "min 3 and max 8") String author, String createDate, String thumbnail, String cateID) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.author = author;
        this.createDate = createDate;
        this.thumbnail = thumbnail;
        this.cateID = cateID;
    }

    @Override
    public String toString() {
        return "Article{" +

                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", author='" + author + '\'' +
                ", createDate='" + createDate + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", cateID='" + cateID + '\'' +
                '}';
    }

    public String getCateID() {
        return cateID;
    }

    public void setCateID(String cateID) {
        this.cateID = cateID;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Article() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }


}
